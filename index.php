<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    
    function echo_animal($animal)
    {
        echo "Name : " . $animal->get_name() . "<br>";
        echo "legs : " . $animal->get_legs() . "<br>";
        echo "cold blooded : " . $animal->get_cold_blooded() . "<br>";
        
        if(is_a($animal, 'Frog'))
        {
            echo "Jump : ";
            echo $animal->jump() . "<br><br>";

        }
        else if(is_a($animal, 'Ape'))
        {
            echo "Yell : ";
            echo $animal->yell() . "<br><br>";
        }
        else
        {
            echo "<br>";
        }
    }

    $sheep = new Animal("shaun");
    echo_animal($sheep);
    
    $kodok = new Frog("buduk");
    echo_animal($kodok);
    
    $sungokong = new Ape("kera sakti");
    echo_animal($sungokong);
?>